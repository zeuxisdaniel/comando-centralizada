import sys
import json
import click

# Clases y variables globales
__author__ = "Zeuxis"
usuario = "mb78113"
separadorPropsConEspacios = "¬"
caracterComentario = "#"
prefijoAap = "servicing.aap.configuration."
arregloAapValidas = ['default', 'unknown']
contextoPorDefaultAap = "ps9-qp0x"
entornos = {"lab" : "cfg.lab.","cdr" : "cfg.cdr.","de" : "cfg.de.","te" : "cfg.te.","qa" : "cfg.qa.","octa" : "cfg.octa.","lo" : "cfg.lo.","pr" : "cfg.pr.","brs" : "cfg.brs.","ei" : "cfg.ei.","au" : "cfg.au.","pi" : "cfg.pi."}
ambitos = {"uuaa" : "A1", "sn" : "A2"}
grupos = {"Logging" : "log", "Otros" : "A04", "Password" : "pwd"}
numeroLinea = 0

@click.group()
def cli():
	pass

class Cambio(object):
	def __init__(self):
		self.typeProperty = None
		self.changeLogs = []
		self.properties = []

cambio = Cambio()

class ChangeLog(object):
	def __init__(self, key):
		self.typeProperty = None
		self.typeChange = None
		self.key = key
		self.changeReason = None
		self.changeUser = usuario
		self.version = None

class PropertyApp(object):
	def __init__(self, key, value, uuaa, group):
		self.typeProperty = "APP"
		self.id = 0
		self.version = None
		self.uuaa = uuaa
		self.key = key
		self.value = value
		self.description = "Alta de propiedad"
		self.group = group
		self.lastChangeDate = None
		self.lastChangeUser = None

class PropertyAap(object):
	def __init__(self, code, key, value):
		self.typeProperty = "AAP"
		self.id = 0
		self.backendContext = contextoPorDefaultAap
		self.version = None
		self.code = code
		self.key = key
		self.value = value
		self.description = "Alta de propiedad Aap"
		self.lastChangeDate = None
		self.lastChangeUser = None

class PropertyArq(object):
	def __init__(self, key, grupo):
		self.typeProperty = "ARCHITECTURE"
		self.id = None
		self.archPropertyVersion = None
		self.key = key
		self.propertyTypes = {
		  "code" : "A1",
		  "description" : "STRING"
		}
		self.group = {
		  "code" : None,
		  "description" : grupo
		}
		self.owner = {
		  "code" : "A01",
		  "description" : "Arq."
		}
		self.description = "Alta de Propiedad"
		self.lastChangeReason = None
		self.lastChangeUser = usuario
		self.propertyValues = []

class PropertyValue(object):
	def __init__(self, value, ambito):
		self.typeProperty = "ARCHITECTUREVALUE"
		self.uuaaOSn = ambito
		self.scope = {
		  "code" : None
		}
		self.value = value
		self.description = "Alta de Propiedad"
		self.changeDescription = None
		self.lastChangeUser = usuario

#Metodos globales
def jsonDefault(object):
	return object.__dict__

def validarRepeticion(ambito, propiedad):
	for propertie in cambio.properties:
		if propiedad[0] == propertie.key:
			for propertyValue in propertie.propertyValues:
				if propertyValue.uuaaOSn == ambito:
					sys.exit("[Error] La propiedad '" + propiedad[0] + "' esta registrada con el mismo ambito: '" + ambito + "', en la linea " + str(numeroLinea))

def crearChangeLogRemoved(nombrePropiedad, tipoPropiedad):
	changelogTmp = ChangeLog(nombrePropiedad)
	changelogTmp.typeChange = "REMOVED"
	changelogTmp.typeProperty = "APP" if (tipoPropiedad == "APP") else "ARCHITECTURE"
	cambio.changeLogs.append(changelogTmp)

def crearObjetosArquitectura(nombrePropiedad, grupo, propertyValue):
	changelogTmp = ChangeLog(nombrePropiedad)
	changelogTmp.typeProperty = "ARCHITECTURE"
	cambio.changeLogs.append(changelogTmp)
	propertyArqTmp = PropertyArq(nombrePropiedad, grupo)
	propertyArqTmp.group["code"] = grupos.get(grupo)
	propertyArqTmp.propertyValues.append(propertyValue)
	cambio.properties.append(propertyArqTmp)

def crearObjetosAplicativo(nombrePropiedad, valorPropiedad, uuaa, grupo):
	changelogTmp = ChangeLog(nombrePropiedad)
	changelogTmp.typeProperty = "APP"
	cambio.changeLogs.append(changelogTmp)
	propertyAppTmp = PropertyApp(nombrePropiedad, valorPropiedad, uuaa, grupo)
	cambio.properties.append(propertyAppTmp)

def crearObjetosAap(code, key, value):
	propertyAapTmp = PropertyAap(code, key, value)
	cambio.properties.append(propertyAapTmp)

def eliminarAap(propiedad):
	if len(propiedad) == 2:
		keyTmp = propiedad[0]
		changelogTmp = ChangeLog(prefijoAap+propiedad[0])
	else:
		keyTmp = propiedad[1]
		changelogTmp = ChangeLog(propiedad[1])
	for propiedadRegistrada in cambio.changeLogs:
		if propiedadRegistrada.key == keyTmp:
			sys.exit("[Error] La propiedad '" + propiedad[0] + "' esta duplicada, en la linea " + str(numeroLinea))
	changelogTmp.typeChange = "REMOVED"
	changelogTmp.typeProperty = "AAP"
	cambio.changeLogs.append(changelogTmp)



def eliminar(propiedad, tipoPropiedad, repetida):
	if repetida:
		sys.exit("[Error] Estas tratando de eliminar la propiedad '" + propiedad[0] + "' que ya tienes registrada para actualizar o dar de alta, en la linea " + str(numeroLinea))
	else:
		crearChangeLogRemoved(propiedad[0], tipoPropiedad)
		indirecciones = propiedad[2:]
		if len(indirecciones) > 0:
			if indirecciones[0] == "todo":
				for indireccionEntorno in entornos.values():
					crearChangeLogRemoved(indireccionEntorno + propiedad[0], tipoPropiedad)
			else:
				for indireccion in indirecciones:
					validarEntorno = entornos.get(indireccion, False)
					if validarEntorno:
						crearChangeLogRemoved(validarEntorno + propiedad[0], tipoPropiedad)
					else:
						sys.exit("[Error] El entorno '" + indireccion + "' esta mal configurado para la propiedad '" + propiedad[0] + "', en la linea " + str(numeroLinea))

def validarObjetosArquitectura(longMinima, valorSinIndireccion, valorAmbito, keyAmbito, grupo, indexInicioIndirecciones, propiedad, repetida):
	if len(propiedad) >= longMinima:
		indireccionesValores = propiedad[longMinima:]
		propertyValue = PropertyValue("${${cfg.prefijo}.${cfg.entorno}." + propiedad[0] + "}" if (len(indireccionesValores) > 0) else valorSinIndireccion, valorAmbito)
		propertyValue.scope["code"] = ambitos.get(keyAmbito)
		if repetida:
			validarRepeticion(valorAmbito, propiedad)
			for propertie in cambio.properties:
				if propertie.key == propiedad[0]:
					propertie.propertyValues.append(propertyValue)
		else:
			crearObjetosArquitectura(propiedad[0], grupo, propertyValue)
		if len(indireccionesValores) > 0:
			indirecciones = propiedad[indexInicioIndirecciones:]
			for index in range(1, len(indirecciones), 2):
				validarEntorno = entornos.get(indirecciones[index - 1], False)
				if validarEntorno:
					duplicada = False
					propertyValueTmp = PropertyValue(indirecciones[index], valorAmbito)
					propertyValueTmp.scope["code"] = ambitos.get(keyAmbito)
					for chlog in cambio.changeLogs:
						if chlog.key == (validarEntorno + propiedad[0]):
							duplicada = True
					if duplicada:
						for propertie in cambio.properties:
							if propertie.key == (validarEntorno + propiedad[0]):
								propertie.propertyValues.append(propertyValueTmp)
					else:
						crearObjetosArquitectura(validarEntorno + propiedad[0], grupo, propertyValueTmp)
				else:
					sys.exit("[Error] El entorno '" + indirecciones[index - 1] + "' esta mal configurado para la propiedad '" + propiedad[0] + "', en la linea " + str(numeroLinea))
	else:
		sys.exit("[Error] La propiedad '" + propiedad[0] + "' no esta registrada correctamente, le hace falta el valor o los valores por entorno, en la linea " + str(numeroLinea))	

def actualizarArq(propiedad, repetida):
	if len(propiedad) >= 3:
		if ambitos.get(propiedad[1], False) :
			if grupos.get(propiedad[3], False):
				if len(propiedad) >= 5:
					validarObjetosArquitectura(5, propiedad[4], propiedad[2], propiedad[1], propiedad[3], 4, propiedad, repetida)
				else:
					sys.exit("[Error] La propiedad '" + propiedad[0] + "' no esta registrada correctamente, le hacen falta argumentos, en la linea " + str(numeroLinea))
			else:
				sys.exit("[Error] La propiedad '" + propiedad[0] + "' no esta registrada correctamente, el valor '" + propiedad[3] + "' no se encuentra dentro de los parametros permitidos de grupo, en la linea " + str(numeroLinea))
		elif grupos.get(propiedad[1], False) :
			validarObjetosArquitectura(3, propiedad[2], "qsrv", "uuaa", propiedad[1], 2, propiedad, repetida)
		else:
			sys.exit("[Error] La propiedad '" + propiedad[0] + "' no esta registrada correctamente, el valor '" + propiedad[1] + "' no se encuentra dentro de los parametros permitidos de ambito o grupo, en la linea " + str(numeroLinea))
	else:
		sys.exit("[Error] La propiedad '" + propiedad[0] + "' no esta registrada correctamente, le hacen falta argumentos, en la linea " + str(numeroLinea))

def actualizarApp(propiedad):
	indireccionesValores = propiedad[2:]
	uuaa = "enax"
	group = "A04"
	indirecciones = propiedad[1:]
	# if (propiedad[1] == "uuaa") and (len(propiedad) >= 4):
	if (propiedad[1] == "uuaa"):
		uuaa = propiedad[2]
		if grupos.get(propiedad[3], False):
			group = grupos.get(propiedad[3])
			indireccionesValores = propiedad[5:]
			valorPropiedad = "${${cfg.prefijo}.${cfg.entorno}." + propiedad[0] + "}" if (len(indireccionesValores) > 0) else propiedad[4]
			indirecciones = propiedad[4:]
		# elif all(elem in sorted(a)  for elem in sorted(propiedad[3])) or all(elem in sorted(propiedad[3])  for elem in sorted(a))
		else:
			indireccionesValores = propiedad[4:]
			valorPropiedad = "${${cfg.prefijo}.${cfg.entorno}." + propiedad[0] + "}" if (len(indireccionesValores) > 0) else propiedad[3]
			indirecciones = propiedad[3:]
		crearObjetosAplicativo(propiedad[0], valorPropiedad, uuaa, group)
	else:
		if grupos.get(propiedad[1], False):
			group = grupos.get(propiedad[1])
			indireccionesValores = propiedad[3:]
			valorPropiedad = "${${cfg.prefijo}.${cfg.entorno}." + propiedad[0] + "}" if (len(indireccionesValores) > 0) else propiedad[2]
			indirecciones = propiedad[2:]
		else:
			indireccionesValores = propiedad[2:]
			valorPropiedad = "${${cfg.prefijo}.${cfg.entorno}." + propiedad[0] + "}" if (len(indireccionesValores) > 0) else propiedad[1]
			indirecciones = propiedad[1:]
		crearObjetosAplicativo(propiedad[0], valorPropiedad, uuaa, group)
	if len(indireccionesValores) > 0:
		for index in range(1, len(indirecciones), 2):
			validarEntorno = entornos.get(indirecciones[index - 1], False)
			if validarEntorno:
				crearObjetosAplicativo(validarEntorno + propiedad[0], indirecciones[index], uuaa, group)
			else:
				sys.exit("[Error] El entorno '" + indirecciones[index - 1] + "' esta mal configurado para la propiedad '" + propiedad[0] + "', en la linea " + str(numeroLinea))

def actualizarAap(propiedad):
	if len(propiedad) == 3:
		validarAap(propiedad[0])
		crearObjetosAap(propiedad[0], propiedad[1], propiedad[2])
	else:
		Aap=extraerAap(propiedad)
		crearObjetosAap(Aap, prefijoAap+propiedad[0], propiedad[1])	

def extraerAap(propiedad):
	TmpAap = propiedad[0].split(".")
	validarAap(TmpAap[0])
	return TmpAap[0]

def validarAap(Aap):
	if not Aap in arregloAapValidas:
		if len(Aap) != 8:
			print("[WARNING] La AAP '" + Aap + "' no tiene 8 caracteres, y no es: " + str(arregloAapValidas))

def main(archivo, tipoPropiedad):
	archivo = open (archivo,'r')
	linea = archivo.readline()
	while linea:
		global numeroLinea
		numeroLinea += 1
		repetida = False
		linea = linea.strip()
		if linea:
			propiedadValores = []
			propiedadesConEspacios = linea.split(separadorPropsConEspacios)
			if len(propiedadesConEspacios) > 1:
				if (len(propiedadesConEspacios) % 2) != 0:
					for index in range(0, len(propiedadesConEspacios), 1):
						if (index % 2) != 0:
							propiedadValores.append(propiedadesConEspacios[index].strip())
						else:
							propiedadValores.extend((propiedadesConEspacios[index].strip()).split())
				else:
					sys.exit("[Error] Hace falta un simbolo '¬' de inicio o cierre, en la linea " + str(numeroLinea))
			else:
				propiedadValores = linea.split()
			if propiedadValores[0][0] != caracterComentario:
				for chlog in cambio.changeLogs:
					if chlog.key == propiedadValores[0]:
						if tipoPropiedad == "APP":
							sys.exit("[Error] La propiedad '" + propiedadValores[0] + "' esta duplicada, en la linea " + str(numeroLinea))
						else:
							if chlog.typeChange == "REMOVED":
								sys.exit("[Error] La propiedad '" + propiedadValores[0] + "' ya fue registrada como eliminada, no se puede actualizar o volver a eliminar, en la linea " + str(numeroLinea))
							else:
								repetida = True
				if len(propiedadValores) >= 2:
					if propiedadValores[1] == "eliminar":
						eliminar(propiedadValores, tipoPropiedad, repetida)
					else:
						if tipoPropiedad == "APP":
							actualizarApp(propiedadValores)
						else:
							actualizarArq(propiedadValores, repetida)
				else:
					sys.exit("[Error] La propiedad '" + propiedadValores[0] + "' no esta registrada correctamente, le hacen falta valores, en la linea " + str(numeroLinea))
		linea = archivo.readline()
	archivo.close()
	cambio.typeProperty = "APP" if (tipoPropiedad == "APP") else "ARCHITECTURE"
	jsonCambio = json.dumps(cambio, default=jsonDefault)
	archivoJson = open("changeLogApp.json" if (tipoPropiedad == "APP") else "changeLogArq.json", "w+")
	archivoJson.write(jsonCambio)
	archivoJson.close()
	click.secho('Se ha generado correctamente el archivo json de salida', fg='green')


def mainAap(archivo):
	archivo = open (archivo,'r')
	linea = archivo.readline()
	while linea:
		global numeroLinea
		numeroLinea += 1
		linea = linea.strip()
		if linea:
			propiedadValores = []
			#tratado de propiedades con espacios
			propiedadesConEspacios = linea.split(separadorPropsConEspacios)
			if len(propiedadesConEspacios) > 1:
				if (len(propiedadesConEspacios) % 2) != 0:
					for index in range(0, len(propiedadesConEspacios), 1):
						if (index % 2) != 0:
							propiedadValores.append(propiedadesConEspacios[index].strip())
						else:
							propiedadValores.extend((propiedadesConEspacios[index].strip()).split())
				else:
					sys.exit("[Error] Hace falta un simbolo '¬' de inicio o cierre, en la linea " + str(numeroLinea))
			else:
				propiedadValores = linea.split()
			if propiedadValores[0][0] != caracterComentario:
				for propiedadRegistrada in cambio.properties:
					if propiedadRegistrada.key == prefijoAap+propiedadValores[0]:
						sys.exit("[Error] La propiedad '" + propiedadValores[0] + "' esta duplicada, en la linea " + str(numeroLinea))
				if len(propiedadValores) == 2:
					if propiedadValores[1] == "eliminar":
						eliminarAap(propiedadValores)
					else:
						actualizarAap(propiedadValores)
				else:
					if len(propiedadValores) == 3:
						if propiedadValores[2] == "eliminar":
							eliminarAap(propiedadValores)
						else:
							actualizarAap(propiedadValores)
		linea = archivo.readline()	
	archivo.close()
	cambio.typeProperty = "AAP"
	jsonCambio = json.dumps(cambio, default=jsonDefault)
	archivoJson = open("changeLogAap.json", "w+")
	archivoJson.write(jsonCambio)
	archivoJson.close()
	click.secho('Se ha generado correctamente el archivo json de salida', fg='green')


#Comandos app y arq
@cli.command()
@click.argument('archivo', type=click.Path(exists=True))
def app(archivo):
	"""
	Crea un archivo json formato aplicativo.

	\b
	- ARCHIVO: Ruta del archivo con las propiedades aplicativas.
	"""
	main(archivo, "APP")

@cli.command()
@click.argument('archivo', type=click.Path(exists=True))
def arq(archivo):
	"""
	Crea un archivo json formato arquitectura.

	\b
	- ARCHIVO: Ruta del archivo con las propiedades de arquitectura.
	"""
	main(archivo, "ARQ")

@cli.command()
@click.argument('archivo', type=click.Path(exists=True))
def aap(archivo):
	"""
	Crea un archivo json formato AAP.

	\b
	- ARCHIVO: Ruta del archivo con las propiedades de arquitectura.
	"""
	mainAap(archivo)