from setuptools import setup

setup(
    name='centralizada-command',
    version='0.1',
    py_modules=['centralizada'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        centralizada=centralizada:cli
    ''',
)